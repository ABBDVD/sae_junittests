# README #


### What is this repository for? ###

* jUnit testing for the SAE static analysis project. Running a bunch of tests without doing any changes on the SAE eclipse project itself
* Version "2" which supports running the tests in console which is slower but more reliable.

### How do I get set up? ###

* Add the project to eclipse. Change the config (static variables) in the Test.java file if necessary.
* Make sure the build path settings are correct.
* [Check the Wiki here](https://bitbucket.org/ABBDVD/sae_junittests/wiki/Guide)

### How do I run/use it? ###

* All .java files to be tested are in the folder src/
* Name them to have the first letter S for Safe and U for Unsafe
* Simply run the jUnit tests.

### Console Setting ###

* Due to memory leaks in the apron / soot libraries some .java files may result as Unsafe despite being safe.
* Using the console setting will prevent this.
* The tests take considerably longer though!

### Contribution guidelines ###

* **Add more .java test files!** YES PLEASE!
* If you see the need to expand something, feel free to do so. Please try not to break it though ;)
* Add more documentation! The wiki and this here are only drafts really.


### Special Thanks ###

* to Benny for adding the console option!