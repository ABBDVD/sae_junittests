package test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import ch.ethz.sae.Verifier;

@RunWith(Parameterized.class)
public class Test {
	
    private static String project_path = "/home/sae/sae_proj2/";
    private static String dir = System.getProperty("user.dir");
    private static final boolean console = false;
    private static final int maxTime = 5;
	
	private String name;
	// constructor for the instance - name is the parameterized argument
	public Test(String name) {
		this.name = name;
	}
	
    @AfterClass
    public static void tearDown() {
    	
    }
	
	// list of all parameters. For each an instance of Test 
	// below will be created, with name assigned to the parameter
	@Parameters(name = "{index}: File {0}")
	public static Collection<String[]> fileArgs() {
		File testFilesDir = new File("src/");
		//File testFilesDir = new File(project_path + "src/"); // in order to get Stest_16_SAFE.java running, move it to the project folder and uncomment this line
		File[] list = testFilesDir.listFiles();
		LinkedList<String[]> fileNames = new LinkedList<String[]>();
		System.out.println("TestFiles found:");
		for (File f: list) {
			String name = f.getName();
			if (f.isFile() && !name.equals("MissileBattery.java")) {
				fileNames.add(new String[]{name.substring(0, name.length()-5)});
				System.out.println(name.substring(0, name.length()-5));
			}
		}
		System.out.println("\n*Refresh the project after adding new java test files if not done in Eclipse.");
		System.out.println("Uncomment in the test method almost at the end of Test.java the annotated line to\nsee the whole soot output for each test case.");
		if (console)
		{
			System.out.println("Some code may be comiled to a different Jimple Program here (e.g. Stest_16_SAFE.java) \n");
			System.out.println("\n\nBuildung Project...");
			String command = "./build.sh";
			String result = runCommandLine(project_path, command);
			System.out.println(result);
		}
		else
		{
			System.out.println("*SLoop.java can result in a runtime error despite being a valid test file. This should be a bug/limitation of the test suite only.\n");
		}
		
		return fileNames;
	}
	
	private static String runCommandLine(String path, String command)
	{
		// run test on command line, Source: http://www.linglom.com/programming/java/how-to-run-command-line-or-execute-external-application-from-java/

		String result = "";
        try {
        	
        	Process pr=Runtime.getRuntime().exec(command,
        	        null, new File(path));
        	
            BufferedReader errorStream = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
            BufferedReader buildResult = new BufferedReader(new InputStreamReader(pr.getInputStream()));

            String line=null;

            while((line=buildResult.readLine()) != null) {
            	result += line + "\n";
            }

            int exitVal = pr.waitFor();
            if (exitVal != 0)
            {
            	result += "Exited with error code "+exitVal+"\n";
                while((line=errorStream.readLine()) != null) {
                	result += line + "\n";
                }
                System.out.println(result);
                throw new Error("Error running the program!");
            }

        } catch(Exception e) {
        	
        	System.out.println("Command: " + command);
            System.out.println("Error: " + e.toString());
            e.printStackTrace();
        }
        return result;
	}
	
	@org.junit.Test
	public void test() throws IOException {
	    long startTime = System.currentTimeMillis();
	    
	    // Execute the verifier
	    
		if (console)
		{
			// run2.sh has to already exist and be executable
			File f = new File(project_path + "run2.sh");
			f.createNewFile();
	        BufferedWriter output = new BufferedWriter(new FileWriter(f));
	        output.write("base=$(pwd) \napron=/home/sae/apron \nexport CLASSPATH=.:$base/soot-2.5.0.jar:$apron/japron/apron.jar:$apron/japron/gmp.jar:$base/bin:" + dir + "/src \nexport LD_LIBRARY_PATH=$apron/lib \njava ch.ethz.sae.Verifier " + name);
	        output.close();
		    String result = runCommandLine(project_path, "./run2.sh");
		    
		    /* */
		    /* Remove comment of next line for full output*/
		     System.out.println(result);
		    /* */
		     
		    //Check if safe or not and assert appropriately
		    if (name.charAt(0) == 'S') {
		    	assertTrue(result.contains("Program " + name + " is SAFE") && ! result.contains("Program " + name + " is UNSAFE"));
		    }
		    else if (name.charAt(0) == 'U') {
		    	assertTrue(result.contains("Program " + name + " is UNSAFE") && ! result.contains("Program " + name + " is SAFE"));
		    }
		    else fail("Please make sure that the file starts with U for Unsafe or S for Safe.");
		}
		else
		{
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    PrintStream ps = new PrintStream(baos);
		    // IMPORTANT: Save the old System.out!
		    PrintStream old = System.out;
		    // Tell Java to use your special stream
		    System.setOut(ps);
		    
		    String[] file = new String[] {name};
		    // Execute the verifier
		    // Print some output: goes to your special stream
			Verifier.main(file);
		    
		    // Put things back
		    System.out.flush();
		    System.setOut(old);
		    
		    
		    /* */
		    /* Remove comment of next line for full output*/
		    System.out.println(baos.toString());
		    /* */
		    
		    
		    // Split the returned message per Line
		    String[] outputPerLine = baos.toString().split("\\r?\\n");
		    //Check if safe or not and assert appropriately
		    if (name.charAt(0) == 'S') {
		    	assertEquals("Program " + file[0] + " is SAFE", outputPerLine[outputPerLine.length-1]);
		    }
		    else if (name.charAt(0) == 'U') {
		    	assertEquals("Program " + file[0] + " is UNSAFE", outputPerLine[outputPerLine.length-1]);
		    }
		    else fail("Please make sure that the file starts with U for Unsafe or S for Safe.");
			
		}
		

      long stopTime = System.currentTimeMillis();
      long elapsedTime = (stopTime - startTime);
      assertTrue(elapsedTime < maxTime * 1000);
	}

}
